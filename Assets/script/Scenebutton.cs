﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scenebutton : MonoBehaviour {

    void Awake()
    {
    }

    public void OXquizbutton()
    {
        Invoke("OXquizstart",0f);

    }

    public void Forgugubutton()
    {
        Invoke("Forgugustart", 0f);
    }

    public void Whilegugubutton()
    {
        Invoke("Whilegugustart", 0f);
    }

    public void Gohomebutton()
    {
        Invoke("Gohomestart", 0f);
    }

    void OXquizstart()
    {
        SceneManager.LoadScene("oxquiz");
    }

    void Forgugustart()
    {
        SceneManager.LoadScene("forgugu");
    }

    void Whilegugustart()
    {
        SceneManager.LoadScene("whilegugu");
    }

    void Gohomestart()
    {
        SceneManager.LoadScene("menu");
    }

}
